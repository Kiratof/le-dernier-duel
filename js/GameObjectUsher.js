class GameObjectUsher {
    constructor() {
        this.filledTile = [];
    }

    placeGameObject(mapGrid, gameObject) {
        var line, column;

        do {
            line = getRandomInt(10);
            column = getRandomInt(10);
        } while (mapGrid[line][column] != 1 || !this.isPositionValid([line, column]));

        this.addFilledTile([line, column]);
        gameObject.setPosition(line, column);
    }

    addFilledTile(position) {
        this.filledTile.push(position)
    }

    getFilledTile() {
        return this.filledTile;
    }

    isPositionValid(position) {
        var isPositionValid = true;

        if (this.isTileTaken(position)) {
            isPositionValid = false
        }
        if (this.isNeighbourTileTaken(position)) {
            isPositionValid = false;
        }

        return isPositionValid;
    }

    isTileTaken(position) {
        var taken = false;
        var takenPositions = this.getFilledTile();

        takenPositions.forEach(takenPosition => {
            if (takenPosition[0] == position[0] && takenPosition[1] == position[1]) {
                taken = true;
            }
        });

        return taken;
    }

    isNeighbourTileTaken(position) {
        var taken = false;
        var takenPositions = this.getFilledTile();

        for (let line = position[0] - 1; line < position[0] + 2; line++) {
            for (let column = position[1] - 1; column < position[1] + 2; column++) {

                takenPositions.forEach(takenPosition => {
                    if (takenPosition[0] == line && takenPosition[1] == column) {
                        taken = true;
                    }
                });
            }
        }

        return taken;
    }
}