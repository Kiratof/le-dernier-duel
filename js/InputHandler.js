mouse = new Mouse();

//Ajout des boutons attaque et défense dans le dom
let attackButtonPlayerOne = document.createElement('button');
attackButtonPlayerOne.innerHTML = 'attaquer';
$('.playerOneController').append(attackButtonPlayerOne);

let defendButtonPlayerOne = document.createElement('button');
defendButtonPlayerOne.innerHTML = 'se défendre';
$('.playerOneController').append(defendButtonPlayerOne);
$('.playerOneController > button').hide();


let attackButtonPlayerTwo = document.createElement('button');
attackButtonPlayerTwo.innerHTML = 'attaquer';
$('.playerTwoController').append(attackButtonPlayerTwo);

let defendButtonPlayerTwo = document.createElement('button');
defendButtonPlayerTwo.innerHTML = 'se défendre';
$('.playerTwoController').append(defendButtonPlayerTwo);
$('.playerTwoController > button').hide();

let playAgainButton = document.createElement('button');
playAgainButton.innerHTML = 'Rejouer';
$('.playAgain').append(playAgainButton);
$('.playAgain > button').hide();

class InputHandler {
    constructor() {
        canvas.addEventListener('mousemove', function (event) {
            mouse.setX(event.offsetX);
            mouse.setY(event.offsetY);
        })

        canvas.addEventListener('click', function (event) {
            game.clickEvent();
        });

        attackButtonPlayerOne.addEventListener('click', function (event) {
            game.playerOne.attack(game.playerTwo);

            changePlayerToPlay();
        });

        attackButtonPlayerTwo.addEventListener('click', function (event) {
            game.playerTwo.attack(game.playerOne);

            changePlayerToPlay();
        });

        defendButtonPlayerOne.addEventListener('click', function (event) {
            game.playerOne.defend();
            changePlayerToPlay();
        });

        defendButtonPlayerTwo.addEventListener('click', function (event) {
            game.playerTwo.defend();
            changePlayerToPlay();
        });

        playAgainButton.addEventListener('click', function (event) {
            location.reload();
        });
    }
}