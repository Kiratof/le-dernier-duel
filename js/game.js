let game = {};
game.map = null;
game.characters = [];
game.weapons = [];
game.objects = [];
game.hasBeenClicked = false;
game.turnToPLay = 0;
game.graph = [];

game.playerOne;
game.playerTwo;
game.winner;

const MOVING_STATE = 'MOVING';
const TRANSITION_STATE = 'TRANSITION';
const FIGHTING_STATE = 'FIGHT';
const ENDGAME_STATE = 'ENDGAME';
game.contexte = MOVING_STATE;

let select = {};

let tileSelector;

game.load = function () {
    console.info("GAME: Chargement des éléments...");

    createMap();
    createGameObjects();
    placeGameObjects();

    this.graph = new Graph(this.map.grid);

    tileSelector = new TileSelector(game.map.TILE_WIDTH, game.map.TILE_HEIGHT);

    select.player = game.characters[game.turnToPLay];

    console.info("GAME: Chargement des éléments terminés");
}

game.update = function (dt) {

    switch (game.contexte) {
        case MOVING_STATE:
            movingState(dt);
            break;

        case TRANSITION_STATE:
            transitionState(dt);
            break;

        case FIGHTING_STATE:
            fightingState(dt);
            break;

        case ENDGAME_STATE:
            endGameState(dt);
            break;

        default:
            break;
    }
}

game.draw = function (pCtx) {

    switch (game.contexte) {
        case MOVING_STATE:
            drawMovingScene(pCtx);
            break;

        case TRANSITION_STATE:
            playTransitionAnimation(pCtx);
            break;

        case FIGHTING_STATE:
            drawFightingScene(pCtx);
            break;

        case ENDGAME_STATE:
            drawEndGameScene(pCtx);
            break;

        default:
            break;
    }
}

function playTransitionAnimation(pCtx) {
    console.info('ANIMATION DE TRANSITION');
}

function drawMovingScene(pCtx) {

    var path = getPathBetweenTwoPosition(select.player.line, select.player.column, tileSelector.getSelectedLine(), tileSelector.getSelectedColumn());
    if (path) {
        //Récupération du joueur 2
        game.characters.forEach(character => {
            if (character != select.player) {
                secondPlayer = character;
            }
        });
        if (!(tileSelector.getSelectedLine() == secondPlayer.line && tileSelector.getSelectedColumn() == secondPlayer.column)) {
            game.map.setPathToDraw(path);
        }
    }

    game.map.draw(pCtx)
    drawGameObjects(pCtx);
}

function endGameState(dt) {
    $('.playerOneController > button').hide();
    $('.playerTwoController > button').hide();
    $('.playAgain > button').fadeIn();

    game.playerOne.fightingBoxPosition = 200;
    game.playerTwo.fightingBoxPosition = 600;

    updatePlayers(dt);

    console.info('Fin du jeu !!');
}

function drawFightingScene(pCtx) {

    pCtx.font = '24px serif';

    //Arrière plan de la zone de combat
    pCtx.fillStyle = '#e9c46a';
    pCtx.fillRect(0, 0, 400, 640);
    pCtx.fillStyle = '#2a9d8f';
    pCtx.fillRect(400, 0, 800, 640);

    //Dessin des éléments
    drawPlayers(pCtx);
    drawScore(pCtx);
}

function drawPlayers(pCtx) {
    game.characters.forEach(player => {
        player.draw(game.map, pCtx);
    });
}

function drawScore(pCtx) {
    pCtx.fillStyle = '#2a9d8f';
    pCtx.fillText("Joueur " + game.playerOne.player, 150, 100);
    pCtx.fillText(game.playerOne.hp, 175, 500);


    pCtx.fillStyle = '#e9c46a';
    pCtx.fillText("Joueur " + game.playerTwo.player, 550, 100);

    pCtx.fillText(game.playerTwo.hp, 575, 500);
}

function drawResult(pCtx) {
    pCtx.font = '24px serif';


    //Couleur du texte
    if (game.winner === game.playerOne) {
        pCtx.fillStyle = '#2a9d8f';
    } else {
        pCtx.fillStyle = '#e9c46a';
    }

    pCtx.fillText("Le vainqueur est le joueur " + game.winner.player, 200, 100);
}

function drawEndGameScene(pCtx) {

    //Arrière plan
    if (game.winner === game.playerOne) {
        pCtx.fillStyle = '#e9c46a';
    } else {
        pCtx.fillStyle = '#2a9d8f';
    }
    pCtx.fillRect(0, 0, 800, 640);

    //Dessin des éléments
    drawPlayers(pCtx);
    drawResult(pCtx);
}



function movingState(dt) {
    updateGameObjects(dt);

    if (game.hasBeenClicked) {
        var player = select.player;
        var secondPlayer;
        var selectedColumn = tileSelector.getSelectedColumn();
        var selectedLine = tileSelector.getSelectedLine();
        var isPlayerHasMoved = false;
        var isMovementValid = false;
        var path = [];

        //Récupération du joueur 2
        game.characters.forEach(character => {
            if (character != player) {
                secondPlayer = character;
            }
        });

        path = getPathBetweenTwoPosition(player.line, player.column, selectedLine, selectedColumn);

        if (path.length <= 3 && path.length > 0 && !(selectedLine == secondPlayer.line && selectedColumn == secondPlayer.column)) {
            isMovementValid = true;
        }

        //Si mouvement valide
        if (isMovementValid) {

            //Ramassage d'une arme au passage s'il y en a une sur le passage t'as vu
            pathToCheck = [];
            path.forEach(tile => {
                pathToCheck.push([tile.x, tile.y]);
            });

            game.weapons.forEach(weapon => {
                pathToCheck.forEach(tile => {

                    if (tile[0] == weapon.line && tile[1] == weapon.column) {
                        let weaponToTake = weapon;
                        let weaponToDrop = player.weapon;
                        weaponToDrop.line = weaponToTake.line;
                        weaponToDrop.column = weaponToTake.column;
                        player.setWeapon(weaponToTake);

                        removeFromArray(game.weapons, weaponToTake);
                        game.weapons.unshift(weaponToDrop);

                    }
                });
            });

            //Vérifier si l'adversaire est pas sur le chemin
            pathToCheck.forEach(tile => {
                //A chaque tuile que l'on traverse
                //On récupère ses voisins
                let neighborsTile = [
                    [tile[0], tile[1] + 1],
                    [tile[0] + 1, tile[1]],
                    [tile[0], tile[1] - 1],
                    [tile[0] - 1, tile[1]],
                ]

                //On vérifie qu'aucun adversaire ne se trouve sur une de ces tuiles
                neighborsTile.forEach(neighbor => {
                    //Si la case est dans la grille
                    if (neighbor[0] >= 0 && neighbor[0] < game.map.MAP_HEIGHT && neighbor[1] >= 0 && neighbor[1] < game.map.MAP_WIDTH) {
                        //Et que le joueur adverse se trouve sur cette case
                        if (neighbor[0] === secondPlayer.line && neighbor[1] === secondPlayer.column) {
                            //On l'arrête sur le case adjacente à l'adversaire
                            selectedLine = tile[0];
                            selectedColumn = tile[1];

                            startFight();

                        }
                    }
                });
            });

            //Déplacement
            movePlayer(player, selectedLine, selectedColumn);

            isPlayerHasMoved = true;
        }

        //CHANGEMENT DU JOUEUR QUI DOIT JOUER
        if (isPlayerHasMoved) {

            if (!(game.contexte === TRANSITION_STATE)) {
                changePlayerToPlay();
            }

            isPlayerHasMoved = false;
        }

        //Interupteur du click    
        game.hasBeenClicked = false;
    }
}

function transitionState(dt) {

    game.playerOne.fightingBoxPosition = 200;
    game.playerTwo.fightingBoxPosition = 600;

    game.contexte = FIGHTING_STATE;
}

function fightingState(dt) {

    updatePlayers(dt);

    //activer/désactiver les actions des joueurs
    if (game.turnToPLay == 0) {
        $('.playerOneController > button').show();
        $('.playerTwoController > button').hide();
    } else {
        $('.playerOneController > button').hide();
        $('.playerTwoController > button').show();
    }

    //Fin de la partie si un joueur atteint 0 hp
    if (game.playerOne.hp === 0 || game.playerTwo.hp === 0) {

        if (game.playerOne.hp === 0) {
            game.winner = game.playerTwo;
        } else {
            game.winner = game.playerOne;
        }

        game.contexte = ENDGAME_STATE;
    }

    console.info('FIGHT TIME');
}

function updatePlayers(dt) {
    game.characters.forEach(player => {
        player.update(dt);
    });
}

function startFight() {
    game.contexte = TRANSITION_STATE;
}

function movePlayer(player, selectedLine, selectedColumn) {
    player.moveTo(selectedLine, selectedColumn);
}

function changePlayerToPlay() {
    if (game.turnToPLay == 0) {
        game.turnToPLay = 1;
        select.player = game.characters[game.turnToPLay];
    } else {
        game.turnToPLay = 0;
        select.player = game.characters[game.turnToPLay];
    }

}

game.clickEvent = function () {
    this.hasBeenClicked = true;
}

function load() {
    game.load();
}

function update(dt) {
    game.update(dt);
}

function draw(pCtx) {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    game.draw(pCtx);
}

function createWeapon(type, line, column) {
    var weapon = new Weapon(dbWeapons[type].image, line, column);
    weapon.type = type;
    weapon.damage = dbWeapons[type].damage;
    game.weapons.push(weapon);

    return weapon;
}

function createPlayer(type, line, column) {
    var player = new Player(dbPlayer[type].image, line, column, dbPlayer[type].animations);
    player.type = type;
    player.player = dbPlayer[type].player;

    let weapon = createWeapon('DAGGER');
    game.weapons.pop();
    player.setWeapon(weapon);

    game.characters.push(player);

    return player;
}

function createMap() {
    game.map = new Map();
    game.map.loadTileSheet('tilesheet-1');
}

function createGameObjects() {
    createWeapons();
    createPlayers();
}

function createPlayers() {
    game.playerOne = createPlayer('PLAYER_ONE', 0, 0);
    game.playerTwo = createPlayer('PLAYER_TWO', 0, 0);
    game.objects.push(game.characters);
}

function createWeapons() {
    createWeapon('AXE', 0, 0);
    createWeapon('HAMMER', 0, 0);
    createWeapon('KATANA', 0, 0);
    createWeapon('TRISHULA', 0, 0);
    game.objects.push(game.weapons);
}

function placeGameObjects() {
    usher = new GameObjectUsher();

    game.objects.forEach(gameObjectContainer => {
        gameObjectContainer.forEach(element => {
            usher.placeGameObject(game.map.grid, element);
        });
    });
}

function updateGameObjects(dt) {
    game.objects.forEach(gameObjectContainer => {
        gameObjectContainer.forEach(gameObject => {
            gameObject.update(dt);
        });
    });
}

function drawGameObjects(pCtx) {
    let selectedColumn = tileSelector.getSelectedColumn();
    let selectedLine = tileSelector.getSelectedLine();

    game.objects.forEach(gameObjectContainer => {
        gameObjectContainer.forEach(gameObject => {
            //Dessin de l'object
            gameObject.draw(game.map, pCtx)

            //Si survolé, présentation des informations de l'objet
            if (gameObject.column == selectedColumn && gameObject.line == selectedLine) {
                gameObject.displayInfo(pCtx);
            }
        });
    });

}

function getPathBetweenTwoPosition(startLinePosition, startColumnPosition, endLinePosition, endColumnPosition) {
    var path = [];

    //Si la tuile selectionné n'est pas un mur, on calcule le chemin
    if (game.map.grid[endLinePosition][endColumnPosition] != 2 &&
        endLinePosition >= 0 &&
        endLinePosition < game.map.MAP_HEIGHT &&
        endColumnPosition >= 0 &&
        endColumnPosition < game.map.MAP_WIDTH) {
        var start = game.graph.grid[startLinePosition][startColumnPosition];
        var end = game.graph.grid[endLinePosition][endColumnPosition];
        path = astar.search(game.graph, start, end);
    }

    return path;
}