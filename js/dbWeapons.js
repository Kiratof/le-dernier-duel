dbWeapons = {}

dbWeapons['AXE'] = {}
dbWeapons['AXE'].damage = 50
dbWeapons['AXE'].image = 'axe';

dbWeapons['DAGGER'] = {}
dbWeapons['DAGGER'].damage = 10
dbWeapons['DAGGER'].image = 'dagger_6';

dbWeapons['HAMMER'] = {}
dbWeapons['HAMMER'].damage = 25
dbWeapons['HAMMER'].image = 'hammer';

dbWeapons['KATANA'] = {}
dbWeapons['KATANA'].damage = 35
dbWeapons['KATANA'].image = 'katana_2';

dbWeapons['TRISHULA'] = {}
dbWeapons['TRISHULA'].damage = 70
dbWeapons['TRISHULA'].image = 'trishula';


