dbPlayer = {}

dbPlayer['PLAYER_ONE'] = {}
dbPlayer['PLAYER_ONE'].image = 'player1';
dbPlayer['PLAYER_ONE'].player = 1;
dbPlayer['PLAYER_ONE'].animations = [];

dbPlayer['PLAYER_ONE'].animations['Idle'] = [
    'martial_1_Idle.png',
    8
]
dbPlayer['PLAYER_ONE'].animations['Attack'] = [
    'martial_1_Attack1.png',
    6
]
dbPlayer['PLAYER_ONE'].animations['Death'] = [
    'martial_1_Death.png',
    6
]



dbPlayer['PLAYER_TWO'] = {}
dbPlayer['PLAYER_TWO'].image = 'player2';
dbPlayer['PLAYER_TWO'].player = 2;
dbPlayer['PLAYER_TWO'].animations = [];
dbPlayer['PLAYER_TWO'].animations['Idle'] = [
    'martial_2_Idle.png',
    4
]
dbPlayer['PLAYER_TWO'].animations['Attack'] = [
    'martial_2_Attack1.png',
    4
]
dbPlayer['PLAYER_TWO'].animations['Death'] = [
    'martial_2_Death.png',
    7
]
