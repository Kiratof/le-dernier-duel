const STATE_IDLE = 0;
const STATE_ATTACK = 1;
const STATE_DEATH = 2;

class Player extends Sprite {
    constructor(pSrc, line, column, animations) {
        super('players/' + pSrc, line, column);
        this.hp = 100;
        this.type = null;
        this.player = null;
        this.weapon = null;
        this.defensivePosture = false;

        this.imgCurrent = 0;
        this.animationSpeed = 10;
        this.nbFrames = [];
        this.nbFrames[0] = animations['Idle'][1];
        this.nbFrames[1] = animations['Attack'][1];
        this.nbFrames[2] = animations['Death'][1];

        this.fightingBoxPosition;
        this.height = 200;
        this.width = 200;

        //Animation
        this.state = STATE_IDLE;

        var imageIdle = new Image();
        imageIdle.src = 'images/players/' + animations['Idle'][0];

        var imageAttack = new Image();
        imageAttack.src = 'images/players/' + animations['Attack'][0];

        var imageDeath = new Image();
        imageDeath.src = 'images/players/' + animations['Death'][0];

        this.images = [
            imageIdle,
            imageAttack,
            imageDeath
        ];

    }

    displayInfo(pCtx, x = 650, y = 25) {
        pCtx.fillText(this.type, x, y);
        pCtx.fillText('Points de vie du joueur : ' + this.hp, x, y * 2);
        pCtx.fillText('Joueur : ' + this.player, x, y * 3);
        this.weapon.displayInfo(pCtx);
    }

    update(dt) {

        this.imgCurrent += (this.animationSpeed * dt);
        if (this.imgCurrent >= this.nbFrames[this.state]) {
            this.imgCurrent = 0;

            if (this.state === STATE_DEATH) {
                this.imgCurrent = this.nbFrames[this.state] - 1;
            }

            if (this.state === STATE_ATTACK) {
                this.changeAnimation(STATE_IDLE);
            }
        }
    }

    draw(pMap, pCtx) {

        if (game.contexte === MOVING_STATE) {

            var frame = Math.floor(this.imgCurrent);
            var sx = this.width * frame;
            var sy = 0;
            var sLargeur = this.width;
            var sHauteur = this.height;


            //var destination = origine tile - centrage de l'image + centrage dans la case
            var dx = (this.column * pMap.TILE_WIDTH) - (this.width / 2) + 32;
            var dy = (this.line * pMap.TILE_HEIGHT) - (this.height / 2) + 32;
            var dLargeur = this.width;
            var dHauteur = this.height;
            pCtx.drawImage(this.images[this.state], sx, sy, sLargeur, sHauteur, dx, dy, dLargeur, dHauteur);
        }

        if (game.contexte === FIGHTING_STATE || game.contexte === ENDGAME_STATE) {
            var frame = Math.floor(this.imgCurrent);
            var sx = this.width * frame;
            var sy = 0;
            var sLargeur = this.width;
            var sHauteur = this.height;

            var dx = this.fightingBoxPosition - (this.width / 2);
            var dy = 320 - (this.height / 2);
            var dLargeur = this.width;
            var dHauteur = this.height;

            if (this.player === 2) {
                //Pour l'effet miroir
                pCtx.save();
                pCtx.scale(-1, 1);
                pCtx.drawImage(this.images[this.state], sx, sy, sLargeur, sHauteur, -dx, dy, -dLargeur, dHauteur);
                pCtx.restore();
            } else {
                pCtx.drawImage(this.images[this.state], sx, sy, sLargeur, sHauteur, dx, dy, dLargeur, dHauteur);
            }
        }
    }

    moveTo(line, colum) {
        this.line = line;
        this.column = colum;
    }

    setWeapon(weapon) {
        this.weapon = weapon;
    }

    attack(player) {
        player.takeDamage(this.weapon.damage);
        this.triggerAttackAnimation();
    }

    triggerAttackAnimation() {
        this.changeAnimation(STATE_ATTACK);
    }

    triggerDeathAnimation() {
        this.animationSpeed = 5;
        this.changeAnimation(STATE_DEATH);
    }

    changeAnimation(state) {
        if (this.state != state) {
            this.state = state;
            this.imgCurrent = 0;
        }
    }

    defend() {
        this.defensivePosture = true;
    }

    takeDamage(damage) {

        if (this.defensivePosture) {
            this.hp -= (damage / 2)
            this.defensivePosture = false;

        } else {
            this.hp -= damage;
        }

        if (this.hp <= 0) {
            this.hp = 0;
            this.triggerDeathAnimation();
        }
    }

}