class Weapon extends Sprite {
    constructor(pSrc, line, column) {
        super('weapons/' + pSrc, line, column);
        this.damage = null;
    }

    displayInfo(pCtx, x = 650, y = 25) {
        pCtx.fillText('Arme : ' + this.type, x, y * 4);
        pCtx.fillText('Dégats : ' + this.damage, x, y * 5);
    }
}