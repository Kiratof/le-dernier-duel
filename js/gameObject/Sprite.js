class Sprite {
    constructor(pSrc, line = 0, column = 0) {
        this.image = new Image();

        this.image.src = 'images/' + pSrc + '.png';
        this.line = line;
        this.column = column;
    }

    update(pMap, dt) {

    }

    draw(pMap, pCtx) {
        var x = this.column * pMap.TILE_WIDTH;
        var y = this.line * pMap.TILE_HEIGHT;

        pCtx.drawImage(this.image, x, y);
    }

    setPosition(line, column) {
        this.line = line;
        this.column = column;
    }
}