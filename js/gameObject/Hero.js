class Hero {
    constructor(line, column) {
        this.images = [];
        this.images[0] = new Image();
        this.images[1] = new Image();
        this.images[2] = new Image();
        this.images[3] = new Image();

        this.images[0].src = 'images/players/hero1.png';
        this.images[1].src = 'images/players/hero2.png';
        this.images[2].src = 'images/players/hero3.png';
        this.images[3].src = 'images/players/hero4.png';
        this.imgCurrent = 0;
        this.line = line;
        this.column = column;
        this.animationSpeed = 5;
    }

    update(pMap, dt) {
        this.imgCurrent = this.imgCurrent + (this.animationSpeed * dt);
        if (this.imgCurrent >= this.images.length) {
            this.imgCurrent = 0;
        }
    }

    draw(pMap, pCtx) {
        var x = this.column * pMap.TILE_WIDTH;
        var y = this.line * pMap.TILE_HEIGHT;

        pCtx.drawImage(this.images[Math.floor(this.imgCurrent)], x, y);
    }

}