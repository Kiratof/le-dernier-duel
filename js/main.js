let canvas = document.getElementById('canvas');
let ctx = canvas.getContext("2d");

let interval;

let derniereUpdate = Date.now();

function calculDeltaTime() {
    let maintenant = Date.now();
    let dt = (maintenant - derniereUpdate) / 1000;
    derniereUpdate = maintenant;
    return dt;
}

function run() {
    update(calculDeltaTime());
    draw(ctx);
}

function init() {
    new InputHandler();
    load();
    interval = setInterval(run, 1000 / 60);
}

init();