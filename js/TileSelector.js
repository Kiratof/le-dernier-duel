class TileSelector {
    constructor(tileWidth, tileHeight) {
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
    }

    getSelectedColumn() {
        let x = mouse.getX();
        let selectedColumn;
        return selectedColumn = Math.floor(x / this.tileWidth);
    }

    getSelectedLine() {
        let y = mouse.getY();
        let selectedLine;
        return selectedLine = Math.floor(y / this.tileHeight);
    }
}