class Map {
    constructor() {
        var mapGenerator = new MapGenerator();
        this.grid = mapGenerator.generateMap();

        this.tilesheet = null;
        this.tileTextures = [];
        this.MAP_WIDTH = 10;
        this.MAP_HEIGHT = 10;
        this.TILE_WIDTH = 64;
        this.TILE_HEIGHT = 64;
        this.tilesheetNbTileLargeur = null;

        this.pathToDraw = [];
    }

    loadTileSheet(nomTileSheet) {
        this.tilesheet = new Image();
        this.tilesheet.src = 'images/tilesheet/' + nomTileSheet + '.png';
        this.tilesheetNbTileLargeur = this.tilesheet.width / this.TILE_WIDTH;
    }

    draw(pCtx) {
        let column, line;

        //Selection d'une tuile
        let selectedColumn = tileSelector.getSelectedColumn();
        let selectedLine = tileSelector.getSelectedLine();

        for (line = 0; line < this.MAP_HEIGHT; line++) {
            for (column = 0; column < this.MAP_WIDTH; column++) {

                //Récupération de l'id et calcul des coordonnées de dessin
                var id = game.map.grid[line][column];
                var xDestination = column * this.TILE_WIDTH;
                var yDestination = line * this.TILE_HEIGHT;

                //Dessin de la tuile
                switch (id) {
                    case 1:
                        pCtx.fillStyle = '#f0efeb';
                        break;

                    case 2:
                        pCtx.fillStyle = '#e76f51';

                        break;

                    default:
                        break;
                }

                pCtx.fillRect(xDestination, yDestination, this.TILE_WIDTH, this.TILE_HEIGHT);
                pCtx.strokeRect(xDestination, yDestination, this.TILE_WIDTH, this.TILE_HEIGHT);

                //Dessin du chemin
                this.pathToDraw.forEach(tile => {
                    if (tile[0] == line && tile[1] == column) {
                        pCtx.fillStyle = '#2a9d8f';
                        pCtx.fillRect(xDestination, yDestination, this.TILE_WIDTH, this.TILE_HEIGHT);
                    }
                });

                //Dessin de la selection
                if (selectedColumn == column && selectedLine == line && selectedColumn != null && selectedLine != null && id != 2) {

                    this.drawSelectionFeedback(pCtx, xDestination, yDestination);
                }
            }
        }
    }

    drawSelectionFeedback(pCtx, xDestination, yDestination) {

        let lineWidth = pCtx.lineWidth;
        let x = xDestination + lineWidth;
        let y = yDestination + lineWidth;
        let width = this.TILE_WIDTH - (lineWidth * 2);
        let height = this.TILE_HEIGHT - (lineWidth * 2);

        pCtx.strokeStyle = '#e76f51';
        pCtx.strokeRect(x, y, width, height);
        pCtx.strokeStyle = '#264653';

    }

    setPathToDraw(path) {
        this.pathToDraw = [];
        if (path.length <= 3 && path.length > 0) {
            path.forEach(tile => {
                this.pathToDraw.push([tile.x, tile.y]);
            });
        }
    }

}