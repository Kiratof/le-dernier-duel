function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function removeFromArray(array, element) {
    for (let i = array.length - 1; i >= 0; i--) {
        if (array[i] == element) {
            array.splice(i, 1);
        }

    }
}