class MapGenerator {
    constructor() {
        this.fillPercent = 80;
        this.GROUND_TILE = 1;
        this.WALL_TILE = 2;

        this.map = [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        ];

        this.height = this.map.length;
        this.width = this.map[0].length;
    }

    generateMap() {
        this.randomFillMap();
        for (let i = 0; i < 20; i++) {

            this.smoothMap();
        }

        return this.map;
    }

    randomFillMap() {
        for (let x = 0; x < this.width; x++) {
            for (let y = 0; y < this.height; y++) {
                if (getRandomInt(100) < this.fillPercent) {
                    this.map[x][y] = this.GROUND_TILE;
                }
                else {
                    this.map[x][y] = this.WALL_TILE;
                }
            }
        }
    }


    smoothMap() {
        let width = this.map[0].length;
        let height = this.map.length;
        for (let x = 0; x < width; x++) {
            for (let y = 0; y < height; y++) {
                let neighbourWallTiles = this.getSurroundingWallCount(x, y);

                if (neighbourWallTiles > 4)
                    this.map[x, y] = 2;
                else if (neighbourWallTiles < 4)
                    this.map[x, y] = 1;

            }
        }
    }

    getSurroundingWallCount(gridX, gridY) {
        let width = this.map[0].length;
        let height = this.map.length;
        let wallCount = 0;
        for (let neighbourX = gridX - 1; neighbourX <= gridX + 1; neighbourX++) {
            for (let neighbourY = gridY - 1; neighbourY <= gridY + 1; neighbourY++) {
                if (neighbourX >= 0 && neighbourX < width && neighbourY >= 0 && neighbourY < height) {
                    if (neighbourX != gridX || neighbourY != gridY) {
                        wallCount += this.map[neighbourX, neighbourY];
                    }
                }
                else {
                    wallCount++;
                }
            }
        }

        return wallCount;
    }

}