class Mouse {
    constructor() {
        this.x = null;
        this.y = null;
    }

    setX(x) {
        this.x = x;
    }
    setY(y) {
        this.y = y;
    }

    getX() {
        return this.x;
    }
    getY() {
        return this.y;
    }

}